<?php

        namespace Drupal\publisso_doi\Form;

        use Drupal\Core\Database\Connection;
        use Drupal\Core\Form\ConfigFormBase;
        use Drupal\Core\Form\FormStateInterface;
        use Symfony\Component\DependencyInjection\ContainerInterface;

        /**
         * TODO: class docs.
         */
        class AdminSettingsForm extends ConfigFormBase {

                /**
                 * The current primary database.
                 *
                 * @var Connection
                 */
                protected $database;

                /**
                 * Creates a AdminSettingsForm instance.
                 *
                 * @param Connection $database
                 *   The current primary database.
                 */
                public function __construct(
                        Connection $database
                ) {
                        $this->database = $database;
                }

                /**
                 * {@inheritdoc}
                 */
                public static function create(ContainerInterface $container) {
                        return new static(
                                $container->get('database')
                        );
                }

                /**
                 * {@inheritdoc}
                 */
                public function getFormId() {
                        return 'publisso_doi_settings_form';
                }

                /**
                 * {@inheritdoc}
                 */
                public function buildForm(array $form, FormStateInterface $form_state) {
                        $form = parent::buildForm($form, $form_state);

                        $config = $this->config('publisso_doi.settings');

                        $form['doi_prefix'] = [
                                '#type' => 'textfield',
                                '#title' => t('DOI prefix'),
                                '#required' => TRUE,
                                '#default_value' => $config->get('doi_prefix'),
                        ];

                        return $form;
                }

                /**
                 * {@inheritdoc}
                 */
                public function validateForm(array &$form, FormStateInterface $form_state) {

                }

                /**
                 * {@inheritdoc}
                 */
                public function submitForm(array &$form, FormStateInterface $form_state) {
                        parent::submitForm($form, $form_state);
                        $config = $this->config('publisso_doi.settings');

                        if ($form_state->hasValue('doi_prefix')) {
                                $config->set('doi_prefix', $form_state->getValue('doi_prefix'));
                        }

                        $config->save();
                }

                /**
                 * {@inheritdoc}
                 */
                protected function getEditableConfigNames() {
                        return ['publisso_doi.settings'];
                }

        }
